### builder
FROM node:18-alpine AS builder
RUN apk add --no-cache libc6-compat
RUN apk update

WORKDIR /app
RUN echo 'loglevel=silent' > .npmrc

RUN npm install -g npm@latest

COPY package*.json .

RUN npm ci
COPY . .
RUN npm run build

### runner
FROM node:18-alpine AS runner

WORKDIR /app

COPY --from=builder /app/dist .

USER node
CMD NODE_OPTIONS='--enable-source-maps' node main.js
