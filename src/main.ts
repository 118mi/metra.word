import { server } from 'server'
import { env, logger } from 'metra.common'
import { setTimeout } from 'node:timers/promises'

import 'common/axios'
import { config } from 'config'
import { db } from 'db'
import { jobs } from 'jobs'
import { WebhookEntity } from 'core/tg/entities/webhook.entity'

env.init(config)

logger.init({
  prettyPrint: env.get('mode') === 'development',
})

let isShutdown = false

const shutdown = async (): Promise<void> => {
  if (isShutdown) return
  isShutdown = true

  try {
    await jobs.stop()
    // logger.info('jobs.stop()')
    console.log('jobs.stop()')
  } catch (e) {
    // logger.error('Jobs stop error.', e)
    console.error('Jobs stop error.')
    console.error(e)
  }

  try {
    await server.stop()
    // logger.info('server.stop()')
    console.log('server.stop()')
  } catch (e) {
    // logger.error('Shutdown application error.', e)
    console.error('Shutdown application error.')
    console.error(e)
  }

  try {
    await db.stop()
    // logger.info('db.stop()')
    console.log('db.stop()')
  } catch (e) {
    console.error('Disconnect db error.')
    console.error(e)
  }

  await setTimeout(1000)
  console.log('shutdown ok')

  process.exit(0)
}

if (env.get('mode') === 'production') {
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  process.on('SIGTERM', shutdown)
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  process.on('SIGINT', shutdown)
}

const main = async (): Promise<void> => {
  try {
    await db.start()
    logger.info('Db was connected.')
  } catch (e) {
    logger.error('Connected db error.', e)
    await shutdown()
  }

  try {
    const address = await server.start()
    logger.info('Server running on: ', address)
  } catch (e) {
    logger.error('Start application error.', e)
    await shutdown()
  }

  try {
    const webhooker = new WebhookEntity({ token: env.get('tgToken') })
    const webhookUrl = WebhookEntity.createWebhookUrl()
    await webhooker.setWebhook(webhookUrl)

    logger.info('webhook was set: ' + webhookUrl)
  } catch (e) {
    logger.error('webhook setting error.', e)
    await shutdown()
  }

  if (env.get('mode') === 'production') {
    try {
      jobs.start()
      logger.info('Jobs was started.')
    } catch (e) {
      logger.error('Start jobs error', e)
      await shutdown()
    }
  }

  logger.info('Application successfully started')
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
main().catch((e) => {
  logger.error('main start error: ', e)
})
