export const config = {
  mode: {
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  port: {
    format: 'port',
    default: 3422,
    env: 'PORT',
  },
  apiHost: {
    default: 'https://metra-word.loca.lt',
    format: String,
    env: 'API_HOST',
  },
  tgToken: {
    format: String,
    default: null,
    env: 'TG_TOKEN',
  },
  notionToken: {
    format: String,
    default: null,
    env: 'NOTION_TOKEN',
  },
  syncItemsCron: {
    format: String,
    default: '0 * * * *',
    env: 'SYNC_ITEMS_CRON',
  },
}
