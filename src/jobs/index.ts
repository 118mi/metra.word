import { setTimeout } from 'node:timers/promises'

import { itemJob } from 'core/item/item.job'

export const jobs = {
  _isStarted: false,

  start(): void {
    if (this._isStarted) return

    itemJob.start()

    this._isStarted = true
  },

  async stop(): Promise<void> {
    if (!this._isStarted) return

    itemJob.stop()

    await setTimeout(1000)

    this._isStarted = false
  },
}
