import { PrismaClient } from '@prisma/client'

class DB {
  private isStarted = false
  private _prisma?: PrismaClient

  get prisma(): PrismaClient {
    if (!this._prisma) throw new Error('start db first')

    return this._prisma
  }

  async start(): Promise<void> {
    if (this.isStarted) return

    this._prisma = new PrismaClient()
    await this._prisma.$connect()

    this.isStarted = true
  }

  async stop(): Promise<void> {
    if (!this.isStarted) return

    await this.prisma.$disconnect()

    this.isStarted = false
  }
}

export const db = new DB()
