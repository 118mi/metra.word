import { PrismaClient } from '@prisma/client'

void (async (): Promise<void> => {
  const prisma = new PrismaClient()
  const t = await prisma.$queryRaw`

    SELECT
      i.item_id "itemId"
      --  ,i.created_at
      --  ,count(chat_item_count_id) cont
    FROM
      item i
      LEFT JOIN chat_item_count cic USING (item_id)
    WHERE
      i.item_id = '3'
    GROUP BY
      i.item_id
    ORDER BY
      count(chat_item_count_id) ASC,
      i.created_at DESC


`
  console.log(t)
})()
