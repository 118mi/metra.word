-- CreateExtension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- CreateEnum
CREATE TYPE "ChatState" AS ENUM ('SetItemsPerDay', 'Word');

-- CreateEnum
CREATE TYPE "MeaningType" AS ENUM ('Verb', 'Noun', 'Adverb', 'Adjective', 'NotWord');

-- CreateTable
CREATE TABLE "chat" (
    "chat_id" VARCHAR(50) NOT NULL,
    "itemsPerDay" SMALLINT NOT NULL DEFAULT 5,
    "hourFrom" SMALLINT NOT NULL DEFAULT 6,
    "hourTo" SMALLINT NOT NULL DEFAULT 18,
    "state" "ChatState" NOT NULL DEFAULT 'Word',
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "chat_pkey" PRIMARY KEY ("chat_id")
);

-- CreateTable
CREATE TABLE "chat_item_ignor" (
    "chat_id" VARCHAR(50) NOT NULL,
    "item_id" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "chat_item_ignor_pkey" PRIMARY KEY ("chat_id","item_id")
);

-- CreateTable
CREATE TABLE "chat_item_count" (
    "chat_item_count_id" UUID NOT NULL DEFAULT uuid_generate_v4(),
    "chat_id" VARCHAR(50) NOT NULL,
    "item_id" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "chat_item_count_pkey" PRIMARY KEY ("chat_item_count_id")
);

-- CreateTable
CREATE TABLE "item" (
    "item_id" VARCHAR(50) NOT NULL,
    "item" VARCHAR NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "item_pkey" PRIMARY KEY ("item_id")
);

-- CreateTable
CREATE TABLE "meaning" (
    "meaning_id" VARCHAR(50) NOT NULL,
    "item_id" VARCHAR(50) NOT NULL,
    "type" "MeaningType" NOT NULL,
    "meaning" VARCHAR NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "meaning_pkey" PRIMARY KEY ("meaning_id")
);

-- CreateTable
CREATE TABLE "example_meaning" (
    "example_id" VARCHAR(50) NOT NULL,
    "meaning_id" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "example_meaning_pkey" PRIMARY KEY ("example_id","meaning_id")
);

-- CreateTable
CREATE TABLE "example" (
    "example_id" VARCHAR(50) NOT NULL,
    "example" VARCHAR NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "example_pkey" PRIMARY KEY ("example_id")
);

-- AddForeignKey
ALTER TABLE "chat_item_ignor" ADD CONSTRAINT "chat_item_ignor_chat_id_fkey" FOREIGN KEY ("chat_id") REFERENCES "chat"("chat_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "chat_item_ignor" ADD CONSTRAINT "chat_item_ignor_item_id_fkey" FOREIGN KEY ("item_id") REFERENCES "item"("item_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "chat_item_count" ADD CONSTRAINT "chat_item_count_chat_id_fkey" FOREIGN KEY ("chat_id") REFERENCES "chat"("chat_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "chat_item_count" ADD CONSTRAINT "chat_item_count_item_id_fkey" FOREIGN KEY ("item_id") REFERENCES "item"("item_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "meaning" ADD CONSTRAINT "meaning_item_id_fkey" FOREIGN KEY ("item_id") REFERENCES "item"("item_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "example_meaning" ADD CONSTRAINT "example_meaning_example_id_fkey" FOREIGN KEY ("example_id") REFERENCES "example"("example_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "example_meaning" ADD CONSTRAINT "example_meaning_meaning_id_fkey" FOREIGN KEY ("meaning_id") REFERENCES "meaning"("meaning_id") ON DELETE RESTRICT ON UPDATE CASCADE;
