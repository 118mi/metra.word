generator client {
  provider        = "prisma-client-js"
  previewFeatures = ["postgresqlExtensions"]
}

datasource db {
  provider   = "postgresql"
  url        = env("DATABASE_URL")
  extensions = [uuidOssp(map: "uuid-ossp")]
}

generator dbml {
  provider              = "prisma-dbml-generator"
  includeRelationFields = false
  projectDatabaseType   = "PostgreSQL"
}

model Chat {
  chatId         String          @id() @map("chat_id") @db.VarChar(50)
  //
  itemsPerDay    Int             @default(5) @db.SmallInt()
  hourFrom       Int             @default(6) @db.SmallInt()
  hourTo         Int             @default(18) @db.SmallInt()
  //
  chatItemIgnors ChatItemIgnor[]
  chatItemCounts ChatItemCount[]
  //
  state          ChatState       @default(Word)
  //
  createdAt      DateTime        @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt      DateTime        @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@map("chat")
}

enum ChatState {
  SetItemsPerDay
  Word
}

model ChatItemIgnor {
  chatId    String   @map("chat_id") @db.VarChar(50)
  chat      Chat     @relation(fields: [chatId], references: [chatId])
  //
  itemId    String   @map("item_id") @db.VarChar(50)
  item      Item     @relation(fields: [itemId], references: [itemId])
  //
  createdAt DateTime @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt DateTime @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@id([chatId, itemId])
  @@map("chat_item_ignor")
}

model ChatItemCount {
  chatItemCountId String   @id() @default(dbgenerated("uuid_generate_v4()")) @map("chat_item_count_id") @db.Uuid()
  //
  chatId          String   @map("chat_id") @db.VarChar(50)
  chat            Chat     @relation(fields: [chatId], references: [chatId])
  //
  itemId          String   @map("item_id") @db.VarChar(50)
  item            Item     @relation(fields: [itemId], references: [itemId])
  //
  createdAt       DateTime @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt       DateTime @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@map("chat_item_count")
}

model Item {
  itemId         String          @id() @map("item_id") @db.VarChar(50)
  //
  item           String          @db.VarChar()
  //
  chatItemIgnors ChatItemIgnor[]
  chatItemCounts ChatItemCount[]
  //
  meanings       Meaning[]
  //
  createdAt      DateTime        @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt      DateTime        @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@map("item")
}

model Meaning {
  meaningId       String           @id() @map("meaning_id") @db.VarChar(50)
  //
  itemId          String           @map("item_id") @db.VarChar(50)
  item            Item             @relation(fields: [itemId], references: [itemId])
  //
  //
  type            MeaningType
  meaning         String           @db.VarChar()
  //
  meaningExamples MeaningExample[]
  //
  createdAt       DateTime         @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt       DateTime         @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@map("meaning")
}

enum MeaningType {
  Verb
  Noun
  Adverb
  Adjective
  NotWord
}

model MeaningExample {
  exampleId String   @map("example_id") @db.VarChar(50)
  example   Example  @relation(fields: [exampleId], references: [exampleId])
  //
  meaningId String   @map("meaning_id") @db.VarChar(50)
  meaning   Meaning  @relation(fields: [meaningId], references: [meaningId])
  //
  createdAt DateTime @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt DateTime @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@id([exampleId, meaningId])
  @@map("example_meaning")
}

model Example {
  exampleId       String           @id() @map("example_id") @db.VarChar(50)
  //
  example         String           @db.VarChar()
  //
  meaningExamples MeaningExample[]
  //
  createdAt       DateTime         @default(now()) @map("created_at") @db.Timestamptz()
  updatedAt       DateTime         @default(now()) @updatedAt() @map("updated_at") @db.Timestamptz()

  @@map("example")
}
