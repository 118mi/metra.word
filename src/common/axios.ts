import axios from 'axios'

interface IFormattedAxiosError {
  statusCode: number
  url: string
  method: string
  message: string
  statusText: string
  responseData: unknown
  headers: Partial<Record<string, string>>
}
const t = JSON.parse('')
class FormattedAxiosError implements IFormattedAxiosError {
  message: string
  statusCode: number
  url: string
  method: string
  statusText: string
  headers: Partial<Record<string, string>>
  responseData: unknown

  constructor(formattedAxiosError: IFormattedAxiosError) {
    this.statusCode = formattedAxiosError.statusCode
    this.message = formattedAxiosError.message
    this.statusText = formattedAxiosError.statusText
    this.responseData = formattedAxiosError.responseData
    this.url = formattedAxiosError.url
    this.headers = formattedAxiosError.headers
    this.method = formattedAxiosError.method
  }
}

// eslint-disable-next-line @typescript-eslint/no-redundant-type-constituents
const normalizeAxiosError = (e: unknown): IFormattedAxiosError | unknown => {
  if (!axios.isAxiosError(e)) return e
  const {
    status,
    config = {
      method: '',
      baseURL: '',
      url: '',
    },
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    data = {},
    statusText = '',
    headers = {},
  } = e.response ?? {}

  return new FormattedAxiosError({
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    headers,
    message: e.message,
    method: config?.method ?? '',
    responseData: data,
    statusCode: status ?? NaN,
    statusText,
    url: (config.baseURL ?? '') + (config.url ?? ''),
  })
}

export const axiosErrorNormalizer = (e: unknown): Promise<never> =>
  Promise.reject(normalizeAxiosError(e))

axios.interceptors.response.use(null, axiosErrorNormalizer)
