import { onRequestHookHandler } from 'fastify'
import { randomUUID } from 'node:crypto'
import { context } from 'metra.common'

export const initRequestContext: onRequestHookHandler = (fastify, _, next) => {
  const traceId = fastify.raw.headers['x-request-id'] ?? randomUUID()
  context.context.run({ traceId }, () => {
    next()
  })
}
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore eslint-ignore
initRequestContext[Symbol.for('skip-override')] = true
