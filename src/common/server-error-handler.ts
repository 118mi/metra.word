import httpError, { HttpError } from 'http-errors'
import { FastifyError } from 'fastify'
import { logger } from 'metra.common'

export const serverErrorHandler = (
  e: FastifyError,
): FastifyError | HttpError => {
  if (httpError.isHttpError(e) || e.validation) {
    return e
  }
  logger.error('Internal server error', e)
  return httpError.InternalServerError()
}
