import { tgRoute } from 'core/tg/routes/intex.route'
import { FastifyPluginAsync } from 'fastify/types/plugin'

export const routes: FastifyPluginAsync = async (fastify) => {
  await fastify.register(tgRoute, { prefix: '/api/v1/webhook' })
}
