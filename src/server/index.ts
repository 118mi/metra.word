import Fastify from 'fastify'
import cors from '@fastify/cors'
import { env } from 'metra.common'

import { initRequestContext } from 'common/request-context-init.hook'
import { routes } from 'server/routes'
import { serverErrorHandler } from 'common/server-error-handler'
import { AddressInfo } from 'net'

class Server {
  private isStarted = false
  private readonly server = Fastify({ logger: false })

  private async setServer(): Promise<void> {
    await this.server.register(cors)

    this.server.setErrorHandler(serverErrorHandler)
    this.server.addHook('onRequest', initRequestContext)

    await this.server.register(routes)
  }

  async stop(): Promise<void> {
    if (!this.isStarted) return

    await this.server.close()

    this.isStarted = false
  }

  async start(): Promise<AddressInfo | string> {
    if (this.isStarted) return this.server.server.address()!

    await this.setServer()
    const s = await this.server.listen({
      port: env.get<number>('port'),
    })
    console.log(s)

    this.isStarted = true

    return this.server.server.address()!
  }
}

export const server = new Server()
