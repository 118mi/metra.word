import { db } from 'db'
import {
  IChat,
  IChatCreate,
  IChatRepository,
  IChatUpdate,
} from 'core/chat/entities/chat.repository.interface'

class ChatRepository implements IChatRepository {
  async getByChatId(chatId: string): Promise<IChat | null> {
    return db.prisma.chat.findUnique({ where: { chatId } })
  }

  async updateByChatId(chatId: string, chat: IChatUpdate): Promise<IChat> {
    return db.prisma.chat.update({ where: { chatId }, data: chat })
  }

  async create(chat: IChatCreate): Promise<IChat> {
    return db.prisma.chat.create({ data: chat })
  }
}

export const chatRepository = new ChatRepository()
