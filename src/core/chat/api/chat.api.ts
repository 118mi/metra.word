import { Api } from 'grammy'

import { IChatApi } from 'core/chat/entities/chat.api.interface'
import { env } from 'metra.common'

class ChatApi implements IChatApi {
  async sendMessage(chatId: string, msg: string): Promise<void> {
    await this.api.sendMessage(chatId, msg)
  }

  private get api(): Api {
    return new Api(env.get('tgToken'))
  }
}

export const chatApi = new ChatApi()
