export interface IChatApi {
  sendMessage(chatId: string, msg: string): Promise<void>
}
