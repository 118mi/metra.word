import httpError from 'http-errors'

import {
  EChatState,
  EMessageTemplate,
  IChat,
  IChatRepository,
} from 'core/chat/entities/chat.repository.interface'
import { chatRepository as postChatRepository } from 'core/chat/repositories/chat.repository'
import { IChatApi } from 'core/chat/entities/chat.api.interface'
import { chatApi as tgChatApi } from 'core/chat/api/chat.api'

export class ChatEntity {
  private readonly chatId: string
  private readonly chatRepository: IChatRepository
  private readonly chatApi: IChatApi

  constructor(
    {
      chatId,
    }: {
      chatId: string
    },
    {
      chatRepository = postChatRepository,
      chatApi = tgChatApi,
    }: {
      chatRepository?: IChatRepository
      chatApi?: IChatApi
    } = {},
  ) {
    this.chatId = chatId
    this.chatRepository = chatRepository
    this.chatApi = chatApi
  }

  async register(): Promise<void> {
    const chatCandidate = await this.chatRepository.getByChatId(this.chatId)
    if (chatCandidate) return
    await this.chatRepository.create({ chatId: this.chatId })
  }

  async setItemsPerDay(itemsPerDayRaw: string): Promise<void> {
    const itemsPerDay = Number.parseInt(itemsPerDayRaw)
    if (!itemsPerDay) {
      return this.sendMessage(EMessageTemplate.IncorrectMessage)
    }

    await this.chatRepository.updateByChatId(this.chatId, {
      itemsPerDay,
      state: EChatState.Word,
    })
  }

  async updateState(state: EChatState): Promise<void> {
    await this.chatRepository.updateByChatId(this.chatId, { state })
  }

  async getState(): Promise<EChatState> {
    return (await this.getChat()).state
  }

  async sendMessage(template: EMessageTemplate): Promise<void> {
    switch (template) {
      case EMessageTemplate.IncorrectMessage: {
        await this.chatApi.sendMessage(this.chatId, 'Некорректное сообщение')
        break
      }
    }
  }

  private async getChat(): Promise<IChat> {
    const chatCandidate = await this.chatRepository.getByChatId(this.chatId)
    if (!chatCandidate) throw httpError.NotFound('chat not found')

    return chatCandidate
  }
}
