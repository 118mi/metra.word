import { Chat, ChatState, Prisma } from '@prisma/client'

export type IChatUpdate = Omit<
  Prisma.ChatUpdateInput,
  'chatItemCounters' | 'chatItemIgnors'
>

export const EChatState = ChatState
export type EChatState = ChatState

export enum EMessageTemplate {
  IncorrectMessage = 'IncorrectMessage',
}

export type IChat = Chat

export type IChatCreate = Omit<
  Prisma.ChatCreateInput,
  'chatItemCounters' | 'chatItemIgnors'
>

export interface IChatRepository {
  getByChatId(chatId: string): Promise<IChat | null>
  updateByChatId(chatId: string, chat: IChatUpdate): Promise<IChat>
  create(chat: IChatCreate): Promise<IChat>
}
