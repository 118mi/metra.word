import { Api } from 'grammy'
import { env } from 'metra.common'

interface IWebhook {
  token: string
}

interface IWebhooker {
  setWebhook(webhookUrl: string): Promise<void>
  removeWebhook(): Promise<void>
  getWebhookUrl(): Promise<string>
}

class TgWebhooker implements IWebhooker {
  private readonly tgApi: Api

  constructor(token: string) {
    this.tgApi = new Api(token)
  }

  async removeWebhook(): Promise<void> {
    await this.tgApi.deleteWebhook()
  }

  getWebhookUrl(): Promise<string> {
    return this.tgApi
      .getWebhookInfo()
      .then((r) => r.url ?? '')
      .catch((e) => {
        console.log(e)
        throw e
      })
  }

  async setWebhook(webhookUrl: string): Promise<void> {
    await this.tgApi.setWebhook(webhookUrl)
  }
}

export class WebhookEntity {
  private readonly webhooker: IWebhooker

  constructor(webhook: IWebhook) {
    this.webhooker = new TgWebhooker(webhook.token)
  }

  async setWebhook(
    webhookUrl: string = WebhookEntity.createWebhookUrl(),
  ): Promise<void> {
    const previousWebhookUrl = await this.getWebhookUrl()
    if (previousWebhookUrl === webhookUrl) return
    return this.webhooker.setWebhook(webhookUrl)
  }

  async getWebhookUrl(): Promise<string> {
    return this.webhooker.getWebhookUrl()
  }

  async removeWebhook(): Promise<void> {
    const previousWebhookUrl = await this.webhooker.getWebhookUrl()
    if (!previousWebhookUrl) return
    return this.webhooker.removeWebhook()
  }

  static createWebhookUrl(): string {
    const url = new URL(`webhook/`, new URL('api/v1/', env.get('apiHost')))
    return url.toString()
  }
}
