import { ITgMessageUseCaseParams } from 'core/tg/tg.types'
import { ChatEntity } from 'core/chat/entities/chat.entity'
import { EChatState } from 'core/chat/entities/chat.repository.interface'

export const tgMessageUseCase = async (
  params: ITgMessageUseCaseParams,
): Promise<void> => {
  const chat = new ChatEntity({ chatId: params.chatId })
  const state = await chat.getState()

  switch (state) {
    case EChatState.SetItemsPerDay: {
      await chat.setItemsPerDay(params.message)
      break
    }
  }
}
