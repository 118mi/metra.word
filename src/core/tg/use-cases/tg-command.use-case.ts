import { ECommands, ITgCommandUseCaseParams } from 'core/tg/tg.types'
import { ChatEntity } from 'core/chat/entities/chat.entity'
import { EChatState } from 'core/chat/entities/chat.repository.interface'

export const tgCommandUseCase = async (
  params: ITgCommandUseCaseParams,
): Promise<void> => {
  switch (params.command) {
    case ECommands.Start: {
      const chat = new ChatEntity({ chatId: params.chatId })
      await chat.register()
      break
    }
    case ECommands.SetItemPerDay: {
      const chat = new ChatEntity({ chatId: params.chatId })
      await chat.updateState(EChatState.SetItemsPerDay)
      break
    }
  }
}
