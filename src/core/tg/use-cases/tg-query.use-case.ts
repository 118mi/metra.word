import { EQueries, ITgQueryUseCase } from 'core/tg/tg.types'
import { ItemHandlerEntity } from 'core/item/entities/item-handler.entity'
import { logger } from 'metra.common'

export const tgQueryUseCase = async (
  params: ITgQueryUseCase,
): Promise<void> => {
  const [action, itemId] = params.query.split('__')

  const itemHandlerEntity = new ItemHandlerEntity({
    chatId: params.chatId,
  })

  switch (action as EQueries) {
    case EQueries.Ignore: {
      if (!itemId) {
        logger.error('[tgQueryUseCase] [EQueries.Ignore] no itemId')
        break
      }
      await itemHandlerEntity.ignore(itemId)
      break
    }
    case EQueries.Unignore: {
      if (!itemId) {
        logger.error('[tgQueryUseCase] [EQueries.Ignore] no itemId')
        break
      }
      await itemHandlerEntity.unignore(itemId)
      break
    }
    case EQueries.NextItem: {
      // получить некст итем
      const nextItem = await itemHandlerEntity.getNextItem()
      console.log(nextItem)
      // отправить через чат энтити
      break
    }
  }
}
