export interface IChatId {
  chatId: string
}

export enum ECommands {
  Start = '/start',
  SetItemPerDay = '/setItemPerDay',
}

export enum EQueries {
  Ignore = 'Ignore',
  Unignore = 'Unignore',
  NextItem = 'NextItem',
}

export interface ITgCommandUseCaseParams extends IChatId {
  command: ECommands
}

export interface ITgMessageUseCaseParams extends IChatId {
  message: string
}

export interface ITgQueryUseCase extends IChatId {
  query: string
}
