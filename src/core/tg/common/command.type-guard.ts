import { ECommands } from 'core/tg/tg.types'

export const commandTypeGuard = (command: string): command is ECommands => {
  return Object.values(ECommands).includes(command)
}
