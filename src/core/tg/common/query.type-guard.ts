import { EQueries } from '../tg.types'

export const queryTypeGuard = (query: string): boolean => {
  return Object.values(EQueries).some((q) => query.startsWith(q))
}
