import { FastifyPluginCallback } from 'fastify'

import { tgController } from 'core/tg/controllers/tg.controller'

export const tgRoute: FastifyPluginCallback = (fastify, _, done) => {
  fastify.post('/', tgController)

  done()
}
