import { FastifyRequest } from 'fastify'
import { Update } from 'grammy/out/types.node'
import { logger } from 'metra.common'

import { tgCommandUseCase } from 'core/tg/use-cases/tg-command.use-case'
import { tgMessageUseCase } from 'core/tg/use-cases/tg-message.use-case'
import { tgQueryUseCase } from 'core/tg/use-cases/tg-query.use-case'
import { queryTypeGuard } from 'core/tg/common/query.type-guard'
import { commandTypeGuard } from 'core/tg/common/command.type-guard'

export const tgController = async (
  req: FastifyRequest<{
    Body: Update
    Params: { botId: string }
  }>,
): Promise<void> => {
  logger.info('tgController req.body', req.body)

  if (req.body.callback_query?.data) {
    if (!queryTypeGuard(req.body.callback_query.data)) return

    return tgQueryUseCase({
      query: req.body.callback_query.data,
      chatId: req.body.callback_query.from.id.toString(),
    })
  }

  if (req.body.message?.text) {
    if (req.body.message.text.startsWith('/')) {
      if (!commandTypeGuard(req.body.message.text)) return

      return tgCommandUseCase({
        command: req.body.message.text,
        chatId: req.body.message.chat.id.toString(),
      })
    } else {
      return tgMessageUseCase({
        message: req.body.message.text,
        chatId: req.body.message.chat.id.toString(),
      })
    }
  }
}
