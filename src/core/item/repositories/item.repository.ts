import {
  IGetItemMeaningExampleParams,
  IItemRepository,
} from 'core/item/entities/item.repository.interface'
import { db } from 'db'

class ItemRepository implements IItemRepository {
  async createChatItemIgnor(chatId: string, itemId: string): Promise<void> {
    await db.prisma.chatItemIgnor.create({ data: { chatId, itemId } })
  }

  async deleteChatItemIgnor(chatId: string, itemId: string): Promise<void> {
    await db.prisma.chatItemIgnor.delete({
      where: {
        chatId_itemId: {
          chatId,
          itemId,
        },
      },
    })
  }

  // async getItemMeaningExamples() {
  //   const itemMeaningExampleRaw = await db.prisma.item.findFirst({
  //     where: {
  //       itemId: item.itemId,
  //     },
  //     include: {
  //       meanings: {
  //         include: {
  //           meaningExamples: {
  //             include: {
  //               example: true,
  //             },
  //           },
  //         },
  //       },
  //     },
  //   })
  // }
  // getItemMeaningExample() {}

  async getNextItemId({
    chatId,
    sortKey,
    sortOrder,
  }: IGetItemMeaningExampleParams): Promise<string | null> {
    const [item] = await db.prisma.$queryRaw<{ itemId: string }[]>`
      SELECT
        i.item_id "itemId"
        --  ,i.created_at
        --  ,count(chat_item_count_id) cont
      FROM
        item i
        LEFT JOIN chat_item_count cic USING (item_id)
      WHERE
        cic.chat_id = ${chatId}
      GROUP BY
        i.item_id
      ORDER BY
        count(chat_item_count_id) ASC,
        i.${sortKey} ${sortOrder}
      LIMIT 1
    `
    return item?.itemId ?? null
  }
}

export const itemRepository = new ItemRepository()
