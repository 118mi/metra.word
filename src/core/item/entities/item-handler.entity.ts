import {
  EItemSortKey,
  EItemSortOrder,
  IItemRepository,
} from 'core/item/entities/item.repository.interface'
import { itemRepository as postItemRepository } from 'core/item/repositories/item.repository'

export class ItemHandlerEntity {
  private readonly itemRepository: IItemRepository
  private readonly chatId: string

  constructor(
    {
      chatId,
    }: {
      chatId: string
    },
    {
      itemRepository = postItemRepository,
    }: {
      itemRepository?: IItemRepository
    } = {},
  ) {
    this.chatId = chatId
    this.itemRepository = itemRepository
  }

  ignore(itemId: string): Promise<void> {
    return this.itemRepository.createChatItemIgnor(this.chatId, itemId)
  }

  unignore(itemId: string): Promise<void> {
    return this.itemRepository.deleteChatItemIgnor(this.chatId, itemId)
  }

  async getNextItem() {
    // todo get last if
    // getItemMeaningExample by filter
    const nextItem = this.itemRepository.getNextItemId({
      chatId: this.chatId,
      sortKey: EItemSortKey.createdAt,
      sortOrder: EItemSortOrder.Desc,
    })
    // sef
    return ''
  }
}
