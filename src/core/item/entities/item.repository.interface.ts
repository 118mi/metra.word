import { Example, Item, Meaning } from '@prisma/client'

export type IExample = Example
export type IMeaning = Meaning
export type IItem = Item
export type IMeaningExample = IMeaning & { examples: IExample[] }
export type ItemMeaningExample = IItem & { meanings: IMeaningExample[] }

export enum EItemSortOrder {
  Asc = 'ASC',
  Desc = 'DESC',
}

export enum EItemSortKey {
  createdAt = 'created_at',
}

export interface IGetItemMeaningExampleParams {
  chatId: string
  sortKey: EItemSortKey
  sortOrder: EItemSortOrder
}

export interface IGetItemMeaningExamplesParams {
  chatIds: string[]
}

export interface IItemRepository {
  createChatItemIgnor(chatId: string, itemId: string): Promise<void>
  deleteChatItemIgnor(chatId: string, itemId: string): Promise<void>
  // getItemMeaningExamples(
  //   params: IGetItemMeaningExamplesParams,
  // ): Promise<IItemMeaningExample[]>
  // getItemMeaningExample(
  //   params: IGetItemMeaningExamplesParams,
  // ): Promise<IItemMeaningExample[]>
  getNextItemId(params: IGetItemMeaningExampleParams): Promise<string | null>
}
