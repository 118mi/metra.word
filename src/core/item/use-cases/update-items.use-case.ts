import { env } from 'metra.common'
import { Client } from '@notionhq/client'

export const updateItemsUseCase = async (): Promise<void> => {
  const notion = new Client({
    auth: env.get('notionToken'),
  })
  const res = await notion.databases.query({
    database_id: '9fb748902f77456c8ec83f803505c004',
  })
  console.log(res)
  // получить итемы
}
