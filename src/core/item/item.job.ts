import { env, logger } from 'metra.common'
import { CronJob } from 'cron'

class ItemJob {
  start(): void {
    this.syncItemsJob.start()
  }

  stop(): void {
    this.syncItemsJob.stop()
  }

  private get syncItemsJob(): CronJob {
    if (!this._syncItemsJob) {
      this._syncItemsJob = new CronJob(env.get('syncItemsCron'), () => {
        logger.info('no getting items')
      })
    }
    return this._syncItemsJob
  }

  private _syncItemsJob?: CronJob
}

export const itemJob = new ItemJob()
