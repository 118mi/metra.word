import { BuildOptions, build } from 'esbuild'
import { copy } from 'esbuild-plugin-copy'

const mode = process.env.NODE_ENV || 'development'

const isProd = mode === 'production'
const outdir = './dist'

const config: BuildOptions = {
  entryPoints: ['./src/main.ts'],
  bundle: true,
  tsconfig: './tsconfig.json',
  minifyWhitespace: isProd,
  minifySyntax: isProd,
  sourcemap: isProd,
  metafile: true,
  platform: 'node',
  outdir,
  target: 'esnext',
  plugins: [
    copy({
      resolveFrom: 'cwd',
      assets: {
        from: [
          './node_modules/.prisma/client/libquery_engine-darwin-arm64.dylib.node',
          './node_modules/.prisma/client/schema.prisma',
        ],
        to: [outdir],
      },
    }),
  ],
}

void (async () => {
  const res = await build(config)
  console.log('build errors: ', res.errors)
})()
